/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.ePortfolioModel;
import view.PortfolioView;

/**
 *
 * @author ddjump
 */
public class FileController {
    
    private boolean saved;
    
    private boolean firstAddTab;
    
    private PortfolioView ui;
    
    private ePortfolioModel portfolioModel;
    
    public FileController(PortfolioView initUI) {
        ui = initUI;
        portfolioModel = ui.getModel();
        saved = true;
        firstAddTab = false;
    }
    
    public void handleNewEPortRequest() {
        ui.updateToolbarControls(saved);
    }
    
    public void handleAddTabRequest() {
        //ui.startEditor();
        portfolioModel.addPage();
    }
    
    public void handleRemoveTabRequest() {
        portfolioModel.removeSelectedPage();
    }
}
