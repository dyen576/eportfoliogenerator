/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.FileController;
import static eportfolio.StartupConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON;
import static eportfolio.StartupConstants.CSS_CLASS_SLIDE_BAR;
import static eportfolio.StartupConstants.CSS_CLASS_SLIDE_SHOW_EDIT_VBOX;
import static eportfolio.StartupConstants.CSS_CLASS_VERTICAL_TOOLBAR_BUTTON;
import static eportfolio.StartupConstants.ICON_ADD_SLIDE;
import static eportfolio.StartupConstants.ICON_EXIT;
import static eportfolio.StartupConstants.ICON_EXPORT;
import static eportfolio.StartupConstants.ICON_LOAD_SLIDE_SHOW;
import static eportfolio.StartupConstants.ICON_NEW_SLIDE_SHOW;
import static eportfolio.StartupConstants.ICON_PAGEEDITOR;
import static eportfolio.StartupConstants.ICON_REMOVE_SLIDE;
import static eportfolio.StartupConstants.ICON_SAVEAS;
import static eportfolio.StartupConstants.ICON_SAVE_SLIDE_SHOW;
import static eportfolio.StartupConstants.ICON_SITEVIEWER;
import static eportfolio.StartupConstants.ICON_VIEW_SLIDE_SHOW;
import static eportfolio.StartupConstants.PATH_ICONS;
import static eportfolio.StartupConstants.STYLE_SHEET_UI;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import static javax.ws.rs.client.Entity.html;
import model.HeaderClass;
import model.ImageClass;
import model.ListClass;
import model.Page;
import model.ParagraphClass;
import model.TextFieldClass;
import model.Workspace;
import model.ePortfolioModel;

/**
 *
 * @author ddjump
 */
public class PortfolioView {
    
    Stage primaryStage;
    Scene primaryScene;
    
    BorderPane eportPane;
    
    FlowPane fileToolbarPane;
    Button newEPort;
    Button loadEPort;
    Button saveEPort;
    Button saveasEPort;
    Button exportEPort;
    Button exitEPort;
    
    //HBox workspace;
    
    VBox EPortEditToolbar;
    Button addTab;
    Button removeTab;
    
    VBox WorkspaceModeToolbar;
    Button pageEditor;
    Button siteViewer;
    
    TabPane tabPane;
    ePortfolioModel eportModel;
    Workspace pageui;
    
    Dialogs dialogs;
    
    //Workspace workspace;
    //ScrollPane EPortEditorScrollPane;
    //VBox EPortEditorPane;
    int selectedPageIndex;
    
    private FileController fileController;
    
    public PortfolioView() {
        eportModel = new ePortfolioModel(this);
        selectedPageIndex = 0;
        tabPane = new TabPane();
        tabPane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
    }
    
    public Stage getWindow() {
        return primaryStage;
    }
    public ePortfolioModel getModel(){
        return eportModel;
    }
    public void startUI(Stage initPrimaryStage, String windowTitle) throws IOException {
       initFileToolbar();
       
       initWorkspace();
       
       initWorkspaceModeToolbar();
       
       initEventHandlers();
       
       dialogs = new Dialogs();
       
       //workspace = new Workspace();
       
       //VM = new ViewMode();
       
       primaryStage = initPrimaryStage;
       initWindow(windowTitle);
    }
    
    private void initWorkspace() {
        //workspace = new HBox();
        eportPane = new BorderPane();
        EPortEditToolbar = new VBox();
        EPortEditToolbar.setSpacing(30);
        EPortEditToolbar.setAlignment(Pos.CENTER);
        EPortEditToolbar.getStyleClass().add(CSS_CLASS_SLIDE_BAR);
        
        addTab = this.initChildButton(EPortEditToolbar,ICON_ADD_SLIDE,CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  false);
        removeTab = this.initChildButton(EPortEditToolbar,ICON_REMOVE_SLIDE,CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  true);
        
        //tabPane = new TabPane();
        eportPane.setCenter(tabPane);
        //EPortEditorPane = new VBox();
        //EPortEditorScrollPane = new ScrollPane(EPortEditorPane);
        //FOR TITLE CODE!
        
        //workspace.getChildren().add(EPortEditToolbar);
        //workspace.getChildren().add(EPortEditorScrollPane);
    }
    
    public void initWorkspaceModeToolbar() {
        WorkspaceModeToolbar = new VBox();
        WorkspaceModeToolbar.setSpacing(30);
        WorkspaceModeToolbar.getStyleClass().add(CSS_CLASS_SLIDE_BAR);
        
        pageEditor = this.initChildButton(WorkspaceModeToolbar,ICON_PAGEEDITOR,CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        siteViewer = this.initChildButton(WorkspaceModeToolbar,ICON_SITEVIEWER,CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        WorkspaceModeToolbar.setAlignment(Pos.CENTER);
    }
    
    private void initEventHandlers() throws IOException {
        fileController = new FileController(this);
        
        newEPort.setOnAction(e -> {
            fileController.handleNewEPortRequest();
            //removeTab.setDisable(false);
            
        });
        
        exitEPort.setOnAction(e -> {
            primaryStage.close();
        });
        
        addTab.setOnAction(e -> {
            fileController.handleAddTabRequest();
            removeTab.setDisable(false);
            
        });
        
        removeTab.setOnAction(e -> {
            fileController.handleRemoveTabRequest();
        });
        
        pageEditor.setOnAction(e -> {
           eportPane.setCenter(tabPane);
            addTab.setDisable(false);
            removeTab.setDisable(false);
        });
        
        siteViewer.setOnAction(e -> {
            try {
                eportPane.setCenter(createWebview());
            } catch (MalformedURLException ex) {
                Logger.getLogger(PortfolioView.class.getName()).log(Level.SEVERE, null, ex);
            }
            //eportPane.setCenter(VM.returnView());
            addTab.setDisable(true);
            removeTab.setDisable(true);
        });
    }
    
    private void initFileToolbar() {
        fileToolbarPane = new FlowPane();
        fileToolbarPane.setHgap(20);
        fileToolbarPane.getStyleClass().add(CSS_CLASS_SLIDE_SHOW_EDIT_VBOX);
        
        newEPort = initChildButton(fileToolbarPane,ICON_NEW_SLIDE_SHOW,CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        loadEPort = initChildButton(fileToolbarPane,ICON_LOAD_SLIDE_SHOW,CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        saveEPort = initChildButton(fileToolbarPane,ICON_SAVE_SLIDE_SHOW,CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
        saveasEPort = initChildButton(fileToolbarPane,ICON_SAVEAS,CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
        exportEPort = initChildButton(fileToolbarPane,ICON_EXPORT,CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        exitEPort = initChildButton(fileToolbarPane,ICON_EXIT,CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
      
        fileToolbarPane.setAlignment(Pos.CENTER);
    }
    
    private void initWindow(String windowTitle) {
        
        primaryStage.setTitle(windowTitle);
        
        Screen screen = Screen.getPrimary();
	Rectangle2D bounds = screen.getVisualBounds();
        
        //primaryStage.setX(bounds.getMinX());
	//primaryStage.setY(bounds.getMinY());
	primaryStage.setWidth(1300);
	primaryStage.setHeight(800);
        
        
        eportPane.setTop(fileToolbarPane);
        eportPane.setId("Pane");
        //ScrollPane scrollPane = new ScrollPane(eportPane);
        //eportPane.getStylesheets().add(STYLE_SHEET_UI);
        
	primaryScene = new Scene(eportPane);
        
        primaryScene.getStylesheets().add(STYLE_SHEET_UI);
	primaryStage.setScene(primaryScene);
	primaryStage.show();
    }
    
    public Button initChildButton(
	    Pane toolbar, 
	    String iconFileName, 
	    String cssClass,
	    boolean disabled) {
	String imagePath = "file:" + PATH_ICONS + iconFileName;
	Image buttonImage = new Image(imagePath);
	Button button = new Button();
	button.getStyleClass().add(cssClass);
	button.setDisable(disabled);
	button.setGraphic(new ImageView(buttonImage));
	toolbar.getChildren().add(button);
	return button;
    }
    
    public void updateToolbarControls(boolean saved) {
        eportPane.setLeft(EPortEditToolbar);
        eportPane.setRight(WorkspaceModeToolbar);
        
        //saveEPort.setDisable(saved);
        //System.out.println("asdfasdfaf");
        dialogs.askForTitle();
   
        updateEPortEditToolbarControls();
    }

    private void updateEPortEditToolbarControls() {
    }
   /* 
    public void startEditor() {
        eportPane.setRight(WorkspaceModeToolbar);
        saveEPort.setDisable(false);
        saveasEPort.setDisable(false);
        workspace.addTab(dialogs.askForTitleTab());
        eportPane.setCenter(workspace.getTabs());
    }
*/

    public void reloadPortfolioPane() throws MalformedURLException {
        System.out.println("inside the method");
        tabPane.getTabs().clear();
        for (Page page : eportModel.getPages()) {
            System.out.println("inside for statement");
            Tab tab = new Tab();
            tab.setText(page.getPageTitle());
            tabPane.getTabs().add(tab);
            tabPane.getSelectionModel().select(selectedPageIndex);
            eportModel.setSelectedPage(eportModel.getPages().get(tabPane.getSelectionModel().getSelectedIndex()));
            System.out.println("index is ---- " + tabPane.getSelectionModel().getSelectedIndex());
            
            pageui = new Workspace(this,eportModel);
            pageui.setPageTitlePrompt(page.getPageTitle());
            pageui.setStudentNamePrompt(page.getStudentName());
            pageui.setFooterPrompt(page.getFooter());
           
            for (HeaderClass header : page.getHeaderModel().getTheHeaders()) {
                System.out.println("went to the header for loop");
                pageui.createHeaderBox(header);
            }
            
            for(ParagraphClass paragraph : page.getParagraphModel().getParagraph()) {
                pageui.createParagraphBox(paragraph);
                System.out.println(paragraph.getParagraph());
            }
            int tfcount = 0;
            for(ListClass list : page.getListModel().getList()) {
                VBox listBox = pageui.createListBox(list);
                
                for(TextFieldClass textfield : list.getTextField()) {
                    listBox.getChildren().add(pageui.createListTextField(textfield, list));
                    tfcount++;
                    System.out.println("data count" + tfcount);
                }
            }
            
            for(ImageClass image : page.getImageModel().getTheImages()) {
                pageui.createImageBox(image);
            }
            
            tab.setContent(pageui);
            System.out.println("past setContent");
        }
        tabPane.setOnMouseClicked(e -> {
            selectedPageIndex  = tabPane.getSelectionModel().getSelectedIndex();
            try {
                this.reloadPortfolioPane();
                /*
                try {
                this.reloadPage();
                } catch (MalformedURLException ex) {
                Logger.getLogger(MainUI.class.getName()).log(Level.SEVERE, null, ex);
                }
                pages.setSelectedPage(pages.getPages().get(selectedPageIndex));
                tabPane.getSelectionModel().select(selectedPageIndex);
                */
            } catch (MalformedURLException ex) {
                Logger.getLogger(PortfolioView.class.getName()).log(Level.SEVERE, null, ex);
            }
	});
    }
    
    public WebView createWebview() throws MalformedURLException{
        WebView browser = new WebView();
        WebEngine webEngine = browser.getEngine();
        Path html = Paths.get("./html/index.html");
        File htmlSource = new File(html.toString());
        webEngine.load(htmlSource.toURI().toURL().toString());
        return browser;
    }
    
    public ePortfolioModel getPages() {
        return eportModel;
    }
    
}
