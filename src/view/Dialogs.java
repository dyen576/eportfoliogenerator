/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import static eportfolio.StartupConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON;
import java.io.IOException;
import java.util.Optional;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.VBox;
import javafx.scene.layout.VBoxBuilder;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import ssm.file.SlideShowFileManager;
import ssm.file.SlideShowSiteExporter;
import ssm.view.SlideShowMakerView;

/**
 *
 * @author ddjump
 */
public class Dialogs {

    private TextInputDialog dialog;
    
    String title;
    
    String titleTab;
    
    String heading;
    
    String paragraph;
    
    public String getTitle() {
        return title;
    }
    
    public String getTitleTab() {
        return titleTab;
    }
    
    public String askForTitle() {
        /*
        System.out.println("hihihihihih");
        JFrame frame = new JFrame();
        System.out.println("alsdhfashdfasfd");
        title = JOptionPane.showInputDialog("Enter your Title:");
        System.out.println("yoooooooooo");
        System.out.println(title);
        //result = title;
        */
        dialog = new TextInputDialog("Enter Text");
        dialog.setTitle("Dialog for Title");
        dialog.setHeaderText("Enter your Title");
 
        Optional<String> result = dialog.showAndWait();
 
        title = result.get();
        return title;
    }
    
    public void openParagraph(String text) {
       
       String returnMe = text;
        
       Stage stage = new Stage();
       VBox vbox = new VBox();
       
       Button okayButton = new Button("Submit");
       
       RadioButton option1 = new RadioButton("Words");
       RadioButton option2 = new RadioButton("Hyperlink");
 
       ToggleGroup group = new ToggleGroup();
       
       option1.setToggleGroup(group);
       option2.setToggleGroup(group);
       
       vbox.getChildren().add(option1);
       vbox.getChildren().add(option2);
       
       Label label1 = new Label("Paragraph:");
       TextArea textField = new TextArea ();
       textField.setText(returnMe);
       textField.setWrapText(true);
       textField.setMaxHeight(400);
       textField.setMaxWidth(400);
       textField.prefHeight(400);
       textField.prefWidth(400);
       vbox.getChildren().addAll(label1, textField);
       vbox.setAlignment(Pos.CENTER);
       vbox.getChildren().add(okayButton);
       vbox.setSpacing(20);
       vbox.setId("workspace");
       
       Scene scene = new Scene(vbox, 450 , 400);
       stage.setScene(scene);
       stage.setMaxHeight(500);
       stage.setMaxWidth(500);
       stage.show();
       
       okayButton.setOnAction(e-> {
           paragraph = textField.getText();
           System.out.println(paragraph);
           stage.close();
       });
    }
    
    public String getParagraph() {
        return paragraph;
    }
    
    public String openlist() {
        String list = "";
        Button add = new Button("+add");
        Button remove = new Button("-Remove");
        add.getStyleClass().add(CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON);
        remove.getStyleClass().add(CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON);
        
        Stage stage = new Stage();
        VBox vbox = new VBox();
        Label label1 = new Label("List:");
        Button okayButton = new Button("Submit");
        
        TextField textField = new TextField ();
        TextField textField2 = new TextField ();
        TextField textField3 = new TextField ();
        TextField textField4 = new TextField ();
        TextField textField5 = new TextField ();
        
        textField.setMaxWidth(375);
        textField2.setMaxWidth(375);
        textField3.setMaxWidth(375);
        textField4.setMaxWidth(375);
        textField5.setMaxWidth(375);
        
        vbox.getChildren().add(add);
        vbox.getChildren().add(remove);
        vbox.getChildren().add(okayButton);
        vbox.getChildren().add(label1);
        vbox.getChildren().addAll(textField);
        vbox.getChildren().addAll(textField2);
        vbox.getChildren().addAll(textField3);
        vbox.getChildren().addAll(textField4);
        vbox.getChildren().addAll(textField5);
        vbox.setSpacing(10);
        vbox.setAlignment(Pos.CENTER);
        
        Scene scene = new Scene(vbox, 450 , 700);
        stage.setScene(scene);
        stage.setMaxHeight(700);
        stage.setMaxWidth(500);
        stage.show();
        
        add.setOnAction(e -> {
            TextField newtextField = new TextField();
            newtextField.setMaxWidth(375);
            vbox.getChildren().addAll(newtextField);
        });
        
        remove.setOnAction(e -> {
            //hbox.clear();
        });
        
        okayButton.setOnAction(e -> {
            stage.close();
        });
        
        return list;
    }
    
    public void openSlideShow() throws IOException {
        Stage stage = new Stage();
        SlideShowFileManager ssf = new SlideShowFileManager();
        SlideShowSiteExporter ssE = new SlideShowSiteExporter();
        SlideShowMakerView slideshowmaker = new SlideShowMakerView(ssf, ssE);
        slideshowmaker.startUI(stage, "Slide Show");
    }
}