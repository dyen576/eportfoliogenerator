/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author ddjump
 */
public class Page {
    
    String title;
    String StudentName;
    String Footer;
    
    HeaderModel HM;
    ParagraphModel PM;
    ListModel LM;
    ImageModel IM;
    
    String IMAGE_PATH;
    
    public Page() {
        
        title = "UNTITLED";
        StudentName = "UNTITLED";
        Footer = "UNTITLED";
        
        HM = new HeaderModel();
        PM = new ParagraphModel();
        LM = new ListModel();
        IM = new ImageModel();
    }
    
    public void setPageTitle(String tabTitle) {
        title = tabTitle;
    }
    
    public String getPageTitle() {
        return title;
    }
    
    
    public String getStudentName() {
        return StudentName;
    }
    
    public void setStudentName(String initStudentName) {
        StudentName = initStudentName;
    }
    
    public String getFooter() {
        return Footer;
    }
    
    public void setFooter(String initFooter) {
        Footer = initFooter;
    }
    
    public HeaderModel getHeaderModel() {
        return HM;
    }
    
    public ParagraphModel getParagraphModel() {
        return PM;
    }
    
    public ListModel getListModel() {
        return LM;
    }
    
    public ImageModel getImageModel() {
        return IM;
    }
}