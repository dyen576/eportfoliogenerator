/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import static eportfolio.StartupConstants.CSS_CLASS_COMPONENT;
import static eportfolio.StartupConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON;
import static eportfolio.StartupConstants.CSS_CLASS_XBUTTON;
import static eportfolio.StartupConstants.ICON_EDIT;
import static eportfolio.StartupConstants.ICON_FINDFILE;
import static eportfolio.StartupConstants.ICON_HEADER;
import static eportfolio.StartupConstants.ICON_IMAGE;
import static eportfolio.StartupConstants.ICON_LINK;
import static eportfolio.StartupConstants.ICON_LIST;
import static eportfolio.StartupConstants.ICON_MINUS;
import static eportfolio.StartupConstants.ICON_PARAGRAPH;
import static eportfolio.StartupConstants.ICON_PLUS;
import static eportfolio.StartupConstants.ICON_SLIDESHOW;
import static eportfolio.StartupConstants.ICON_VIDEO;
import static eportfolio.StartupConstants.PATH_ICONS;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import view.Dialogs;
import view.PortfolioView;

/**
 *
 * @author ddjump
 */
public class Workspace extends HBox{
    
    PortfolioView eportView;
    ePortfolioModel eportModel;
    
    Workspace controller;
    
    String title;
    String studentName;
    String footer;
    
    Button bannerImage;
    Button HeadingBttn;
    Button ParagraphBttn;
    Button ListBttn;
    Button ImageBttn;
    Button VideoBttn;
    Button SlideShowBttn;
    
    Button delete;
    Button editButton;
    Button linkButton;
    Button plus;
    Button minus;
    
    String IMAGE_PATH;
    
    ScrollPane scrollPane;
    HBox space;
    VBox editSpace;
    VBox editColumn;
    
    HBox upperPart;
    VBox textFieldBox;
    VBox selectionBox;
    HBox componentBox;
    VBox pControl;
    URL url;
    
    Dialogs dialogs;
    
    TextField textField;
    TextField textField2;
    TextField textField3 ;
    
    TextField headerTextField;
    
    public Workspace(PortfolioView PV, ePortfolioModel PM) {
        eportView = PV;
        eportModel = PM;
        
        space = new HBox();
        
        editColumn = new VBox();
        editColumn.setAlignment(Pos.CENTER);
        editColumn.setSpacing(30);
        editColumn.setId("workspace");
        editColumn.setPrefWidth(750);
        
        editSpace = new VBox();
        editSpace.setAlignment(Pos.CENTER);
        editSpace.setSpacing(30);
        editSpace.setPrefWidth(750);
        
        scrollPane = new ScrollPane(editColumn);
        scrollPane.setFitToWidth(true);
        scrollPane.setFitToHeight(true);
        
        space.getChildren().add(editSpace);
        space.getChildren().add(scrollPane);
        
        upperPart = new HBox();
        upperPart.setSpacing(30);
        upperPart.setAlignment(Pos.CENTER);
        
        textFieldBox = new VBox();
        selectionBox = new VBox();
        componentBox = new HBox();
       
        dialogs = new Dialogs();

        textField();
        selectionField();
        componentField();
        initEventHandlers();
        
        this.getChildren().add(space);
    }
    
    public HBox getEditspace() {
        return space;
    }
    
    public void textField() {
        Label label1 = new Label("Page Title: ");
        Label label2 = new Label("Student Name: ");
        Label label3 = new Label("Footer: ");
        
        textField = new TextField ();
        textField2 = new TextField ();
        textField3 = new TextField ();
        
        //HBox hb = new HBox();
        textFieldBox.getChildren().addAll(label1, textField);
        textFieldBox.getChildren().addAll(label2, textField2);
        textFieldBox.getChildren().addAll(label3, textField3);
        
        //textFieldBox.setSpacing(10);
        textFieldBox.setId("component");
        upperPart.getChildren().add(textFieldBox);
        //editSpace.getChildren().add(upperPart);
        
        textField.setOnAction(e -> {
            System.out.println("textfield read " +  textField.getText());
            
            //eportView.getPages().getSelectedPage().setPageTitle(textField.getText());
            eportView.getPages().getSelectedPage().setPageTitle(textField.getText());
            System.out.println("----- " + eportView.getPages().getSelectedPage().getPageTitle());
            try {
                //textField.setPromptText(eportView.getPages().getSelectedPage().getPageTitle());
                eportView.reloadPortfolioPane();
            } catch (MalformedURLException ex) {
                Logger.getLogger(Workspace.class.getName()).log(Level.SEVERE, null, ex);
            }
	});
        
        textField2.setOnAction(e -> {
            eportView.getPages().getSelectedPage().setStudentName(textField2.getText());
            try {
                eportView.reloadPortfolioPane();
            } catch (MalformedURLException ex) {
                Logger.getLogger(Workspace.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        
        textField3.setOnAction(e -> {
            eportView.getPages().getSelectedPage().setFooter(textField3.getText());
            try {
                eportView.reloadPortfolioPane();
            } catch (MalformedURLException ex) {
                Logger.getLogger(Workspace.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }
    
    public void selectionField() {
        bannerImage = new Button("Banner Image");
        selectionBox.getChildren().add(bannerImage);
        
        ObservableList<String> options1 = 
        FXCollections.observableArrayList(
            "Layout 1",
            "Layout 2",
            "Layout 3",
            "Layout 4",
            "Layout 5"
        );
        ComboBox comboBox = new ComboBox(options1);
        comboBox.setPromptText("Layout    ");
        selectionBox.getChildren().add(comboBox);
        
        ObservableList<String> options2 = 
        FXCollections.observableArrayList(
            "Color 1",
            "Color 2",
            "Color 3",
            "Color 4",
            "Color 5"
        );
        ComboBox comboBox2 = new ComboBox(options2);
        comboBox2.setPromptText("Color Mode");
        selectionBox.getChildren().add(comboBox2);
        
        ObservableList<String> options3 = 
        FXCollections.observableArrayList(
            "Font 1",
            "Font 2",
            "Font 3",
            "Font 4",
            "Font 5"
        );
        ComboBox comboBox3 = new ComboBox(options3);
        comboBox3.setPromptText("Page Font ");
        selectionBox.getChildren().add(comboBox3);
        
        selectionBox.setSpacing(20);
        selectionBox.setId("component");
        
        upperPart.getChildren().add(selectionBox);
        editSpace.getChildren().add(upperPart);
    }
    
    public void componentField() {
        HeadingBttn = initChildButton(ICON_HEADER, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        ParagraphBttn = initChildButton(ICON_PARAGRAPH, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        ListBttn = initChildButton(ICON_LIST, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        ImageBttn = initChildButton(ICON_IMAGE, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        VideoBttn = initChildButton(ICON_VIDEO, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        SlideShowBttn = initChildButton(ICON_SLIDESHOW, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        
        componentBox.getChildren().add(HeadingBttn);
        componentBox.getChildren().add(ParagraphBttn);
        componentBox.getChildren().add(ListBttn);
        componentBox.getChildren().add(ImageBttn);
        componentBox.getChildren().add(VideoBttn);
        componentBox.getChildren().add(SlideShowBttn);
        
        //componentBox.setSpacing(10);
        componentBox.setMaxWidth(200);
        componentBox.setAlignment(Pos.CENTER);
        componentBox.setId("component");
        editSpace.getChildren().add(componentBox);
    }
    
    public void setPageTitlePrompt(String prompt) {
        textField.setPromptText(prompt);
    }
    
    public void setStudentNamePrompt(String prompt) {
        textField2.setPromptText(prompt);
    }
    
    public void setFooterPrompt(String prompt) {
        textField3.setPromptText(prompt);
    }
    
    public Button initChildButton(
	    String iconFileName, 
	    String cssClass,
	    boolean disabled) {
	String imagePath = "file:" + PATH_ICONS + iconFileName;
	Image buttonImage = new Image(imagePath);
	Button button = new Button();
	button.getStyleClass().add(cssClass);
	button.setDisable(disabled);
	button.setGraphic(new ImageView(buttonImage));
	return button;
    }
    
    private void initEventHandlers() {
        HeadingBttn.setOnAction(e -> {
           HeaderClass h = eportModel.getSelectedPage().getHeaderModel().addHeader();
           createHeaderBox(h);
            //eportView.reloadPortfolioPane();
        });
        
        ParagraphBttn.setOnAction(e -> {
            ParagraphClass p = eportModel.getSelectedPage().getParagraphModel().addParagraph();
            //Dialogs d = new Dialogs();
            createParagraphBox(p);
        });
        
        ListBttn.setOnAction(e -> {
            ListClass l = eportModel.getSelectedPage().getListModel().addList();
            createListBox(l);
        });
        
        ImageBttn.setOnAction(e -> {
            ImageClass I = eportModel.getSelectedPage().getImageModel().addImage();
            try {
                createImageBox(I);
            } catch (MalformedURLException ex) {
                Logger.getLogger(Workspace.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        
        VideoBttn.setOnAction(e -> {
            
        });
        
        SlideShowBttn.setOnAction(e -> {
            try {
                dialogs.openSlideShow();
            } catch (IOException ex) {
                Logger.getLogger(Page.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }
    
    public void createHeaderBox(HeaderClass HS) {
        Button dummyHeader = new Button();
        dummyHeader = initChildButton(ICON_HEADER, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
       // headerTextField = new TextField();
        TextField tf = new TextField();
        tf.setPromptText(HS.getHeader());
        delete = new Button("X");
        delete.getStyleClass().add(CSS_CLASS_XBUTTON);
        HBox content = new HBox();
        content.setAlignment(Pos.CENTER);
        content.setMaxWidth(400);
        content.getChildren().add(dummyHeader);
        content.getChildren().add(tf);
        content.getChildren().add(delete);
        content.setId("component");
        editColumn.getChildren().add(content);
       
        delete.setOnAction(e -> {
           editColumn.getChildren().remove(content);
           eportView.getPages().getSelectedPage().getHeaderModel().setSelectedHeader(HS);
           eportView.getPages().getSelectedPage().getHeaderModel().removeSelectedHeader();
            try {
                eportView.reloadPortfolioPane();
            } catch (MalformedURLException ex) {
                Logger.getLogger(Workspace.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        
        /**
        headerTextField.setOnAction(e -> {
            //eportView.getPages().getSelectedPage().setPageTitle(textField.getText());
            //eportView.getPages().getSelectedPage().getHeaderModel().setSelectedHeader(initSelectedHeader);
            eportView.getPages().getSelectedPage().getHeaderModel().getSelectedHeader().setHeader(headerTextField.getText());
            System.out.println(headerTextField.getText());
            //eportView.reloadPortfolioPane();
        });
        * 
        */
        tf.setOnAction(e -> {
            //eportView.getPages().getSelectedPage().setPageTitle(textField.getText());
            eportView.getPages().getSelectedPage().getHeaderModel().setSelectedHeader(HS);
            eportView.getPages().getSelectedPage().getHeaderModel().getSelectedHeader().setHeader(tf.getText());
            try {
                //System.out.println(tf.getText()+"11111      "+ HS.getHeader());
                eportView.reloadPortfolioPane();
            } catch (MalformedURLException ex) {
                Logger.getLogger(Workspace.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }
    
    public void createParagraphBox(ParagraphClass PC) {
        
        TextArea p = new TextArea();
        String para = new String();
        
        Button dummyParagraph = new Button();
        editButton = new Button();
        
        pControl = new VBox();
        dummyParagraph = initChildButton(ICON_PARAGRAPH, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        editButton = initChildButton(ICON_EDIT, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        linkButton = initChildButton(ICON_LINK, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        
        delete = new Button("X");
        delete.getStyleClass().add(CSS_CLASS_XBUTTON);
        
        p.setText(PC.getParagraph());
        HBox content = new HBox();
        //content.setAlignment(Pos.CENTER);
        content.setMaxWidth(500);
        content.setMaxHeight(800);
        p.setWrapText(true);
        p.setMaxHeight(400);
        p.setMaxWidth(400);
        
        content.getChildren().add(dummyParagraph);
        content.getChildren().add(p);
        pControl.getChildren().add(editButton);
        pControl.getChildren().add(linkButton);
        pControl.setSpacing(10);
        content.getChildren().add(pControl);
        content.getChildren().add(delete);
        content.setId("component");
        editColumn.getChildren().add(content);
        
        p.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ENTER)  {
                    String text = p.getText();
                    eportView.getPages().getSelectedPage().getParagraphModel().setSelectedParagraph(PC);
                    eportView.getPages().getSelectedPage().getParagraphModel().getSelectedParagraph().setParagraph(p.getText());
                    try {
                        eportView.reloadPortfolioPane();
                    } catch (MalformedURLException ex) {
                        Logger.getLogger(Workspace.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
        
        delete.setOnAction(e -> {
           editColumn.getChildren().remove(content);
           eportView.getPages().getSelectedPage().getParagraphModel().setSelectedParagraph(PC);
           eportView.getPages().getSelectedPage().getParagraphModel().removeSelectedParagraph();
            try {
                eportView.reloadPortfolioPane();
            } catch (MalformedURLException ex) {
                Logger.getLogger(Workspace.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        
        editButton.setOnAction(e -> {
        });
        
        linkButton.setOnAction(e -> {
        });
    }
    
    public VBox createListBox(ListClass LC) {
        Button dummyList = initChildButton(ICON_LIST, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        plus = initChildButton(ICON_PLUS, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        //minus = initChildButton(ICON_MINUS, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        //deletePart = new Button("x");
        
        delete = new Button("X");
        delete.getStyleClass().add(CSS_CLASS_XBUTTON);
        
        HBox content = new HBox();
        VBox listBox = new VBox();
        VBox plusminus = new VBox();
        
        plusminus.setAlignment(Pos.CENTER);
        plusminus.setSpacing(10);
        plusminus.getChildren().add(dummyList);
        
        plusminus.getChildren().add(plus);
        content.setAlignment(Pos.CENTER);
        content.getChildren().add(plusminus);
        content.getChildren().add(listBox);
        content.getChildren().add(delete);
        content.setId("component");
        
        editColumn.getChildren().add(content);
        
        delete.setOnAction(e -> {
            eportView.getPages().getSelectedPage().getListModel().setSelectedList(LC);
            eportView.getPages().getSelectedPage().getListModel().removeSelectedList();
            editColumn.getChildren().remove(content);
        });
        
        plus.setOnAction(e -> {
            eportView.getPages().getSelectedPage().getListModel().setSelectedList(LC);
            TextFieldClass tfClass = new TextFieldClass();
            eportView.getPages().getSelectedPage().getListModel().getSelectedList().addTextField(tfClass);
            listBox.getChildren().add(createListTextField(tfClass,LC));
        });
        
        return listBox;
    }
    
    public HBox createListTextField(TextFieldClass data, ListClass lc) {
        HBox tfBox = new HBox();
        Button deletePart = new Button("x");
        TextField newtextField = new TextField();
        newtextField.setPromptText(data.getTextField());
        tfBox.getChildren().add(newtextField);
        tfBox.getChildren().add(deletePart);
        
        deletePart.setOnAction(e -> {
            eportView.getPages().getSelectedPage().getListModel().setSelectedList(lc);
            eportView.getPages().getSelectedPage().getListModel().getSelectedList().setSelectedTextField(data);
            eportView.getPages().getSelectedPage().getListModel().getSelectedList().removeSelectedTextField();
            tfBox.getChildren().clear();
        });
        
        newtextField.setOnAction(e -> {
            eportView.getPages().getSelectedPage().getListModel().setSelectedList(lc);
            eportView.getPages().getSelectedPage().getListModel().getSelectedList().setSelectedTextField(data);
            data.setTextField(newtextField.getText());
            System.out.println(newtextField.getText());
        });
        return tfBox;
    }
    
    public void createImageBox(ImageClass IC) throws MalformedURLException {
        Button dummyImage = initChildButton(ICON_IMAGE, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        Button folder = initChildButton(ICON_FINDFILE, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        HBox imageSpace = new HBox();
        
        Button delete = new Button("X");
        delete.getStyleClass().add(CSS_CLASS_XBUTTON);
        HBox content = new HBox();
        Image image = new Image(IC.getImagePath());
        ImageView imageView = new ImageView(image);
        imageView.setFitWidth(200);
        imageView.setFitHeight(150);
        imageSpace.getChildren().add(imageView);
        content.setAlignment(Pos.CENTER);
        content.setMaxWidth(400);
        content.getChildren().add(dummyImage);
        content.getChildren().add(folder);
        content.getChildren().add(imageSpace);
        content.getChildren().add(delete);
        content.setId("component");
        editColumn.getChildren().add(content);
        
        delete.setOnAction(e -> {
           editColumn.getChildren().remove(content);
           eportView.getPages().getSelectedPage().getImageModel().setSelectedImage(IC);
           eportView.getPages().getSelectedPage().getImageModel().removeSelectedImage();
            try {
                eportView.reloadPortfolioPane();
            } catch (MalformedURLException ex) {
                Logger.getLogger(Workspace.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        
        folder.setOnAction(e ->{
            try {
                imageSpace.getChildren().remove(imageView);
                String urlString = FindFile();
                URL newURL = new URL(urlString);
                
                eportView.getPages().getSelectedPage().getImageModel().setSelectedImage(IC);
                eportView.getPages().getSelectedPage().getImageModel().getSelectedImage().setImagePath(urlString);
                
                Image newImage = new Image(newURL.toExternalForm());
                ImageView newImageView = new ImageView(newImage);
                newImageView.setFitWidth(200);
                newImageView.setFitHeight(150);
                newImageView.setPreserveRatio(true);
                imageSpace.getChildren().add(newImageView);
            } catch (MalformedURLException ex) {
                Logger.getLogger(Workspace.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }
    
    public void createVideoBox() {
        delete = new Button("X");
        delete.getStyleClass().add(CSS_CLASS_XBUTTON);
        Button dummylist = initChildButton(ICON_LIST, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        HBox content = new HBox();
        content.setAlignment(Pos.CENTER);
        VBox image = new VBox();
        content.getChildren().add(dummylist);
        content.getChildren().add(delete);
        content.setId("component");
        editSpace.getChildren().add(content);
        
        delete.setOnAction(e -> {
           content.getChildren().clear();
        });
        
    }
    
    public String FindFile(){
        FileChooser imageFileChooser = new FileChooser();
        File selectedFile = imageFileChooser.showOpenDialog(eportView.getWindow());
            try {
                url = selectedFile.toURI().toURL();
            } catch (MalformedURLException ex) {
            }
        if(selectedFile != null) {
            String path = selectedFile.getPath().substring(0, selectedFile.getPath().indexOf(selectedFile.getName()));
            String fileName = selectedFile.getName();
        }
        String returnMe = url.toString();
        return returnMe;
     }
}