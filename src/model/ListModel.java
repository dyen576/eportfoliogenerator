/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author ddjump
 */
public class ListModel {
    ObservableList<ListClass> theList;
    ListClass selectedList;
    
    public ListModel() {
        theList = FXCollections.observableArrayList();
    }
    
    public ObservableList<ListClass> getList() {
        return theList;
    }
    
    public ListClass getSelectedList() {
        return selectedList;
    }
    
    public void setSelectedList(ListClass initSelectedList) {
        selectedList = initSelectedList;
    }
   
    public ListClass addList() {
        ListClass oneList = new ListClass();
        theList.add(oneList);
        
        return oneList;
    }
    
    public void removeSelectedList() {
        theList.remove(selectedList);
    }
}