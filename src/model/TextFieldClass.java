/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author ddjump
 */
public class TextFieldClass {
    String textField;
    
    public TextFieldClass() {
        textField = "";
    }
    
    public String getTextField() {
        return textField;
    }
    
    public void setTextField(String newTextField) {
        textField = newTextField;
    }
}
