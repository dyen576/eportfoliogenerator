/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.net.MalformedURLException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.HBox;
import view.PortfolioView;

/**
 *
 * @author ddjump
 * 
 * 
 */
public class ePortfolioModel {
    PortfolioView ui;
    ObservableList<Page> thePages;
    Page selectedPage;
    String title;
    
    public ePortfolioModel(PortfolioView initUI){ 
        ui = initUI;
        thePages = FXCollections.observableArrayList();
    }
    
    public boolean isPageSelected() {
        return selectedPage != null;
    }
    
    public boolean isSelectedPage(Page testPage) {
        return selectedPage == testPage;
    }
    
    public ObservableList<Page> getPages() {
        return thePages;
    }
    
    public Page getSelectedPage() {
        return selectedPage;
    }
    
    public void setSelectedPage(Page initSelectedPage) {
       selectedPage = initSelectedPage; 
    }
    
    public String getTitle() {
        return title;
    }
    
    public void setTitle(String initTitle) {
        title = initTitle;
    }
    
    public void reset() {
        thePages.clear();
        selectedPage = null;
    }
    
    public void addPage() throws MalformedURLException {
        Page aPage = new Page();
        thePages.add(aPage);
        System.out.println("im here");
        ui.reloadPortfolioPane();
    }
    
    public void removeSelectedPage() throws MalformedURLException {
        thePages.remove(selectedPage);
        ui.reloadPortfolioPane();
    }   
}
