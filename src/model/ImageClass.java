/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author ddjump
 */
public class ImageClass {
    String imagePath;
    
    public ImageClass() {
        imagePath = "DefaultStartSlide.png";
    }
    
    public String getImagePath() {
        return imagePath;
    }
    
    public void setImagePath(String newImagePath) {
        imagePath = newImagePath;
    }
}
