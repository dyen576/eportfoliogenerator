/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author ddjump
 */
public class ParagraphClass {
    String paragraph;
    
    public ParagraphClass() {
        paragraph = "";
    }
    
    public String getParagraph() {
        return paragraph;
    }
    
    public void setParagraph(String newParagraph) {
        paragraph = newParagraph;
    }
}
