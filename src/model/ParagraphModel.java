/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author ddjump
 */
public class ParagraphModel {
    
    ObservableList<ParagraphClass> theParagraphs;
    ParagraphClass selectedParagraph;
    
    public ParagraphModel() {
        theParagraphs = FXCollections.observableArrayList();
    }
    
    public ObservableList<ParagraphClass> getParagraph() {
        return theParagraphs;
    }
    
    public ParagraphClass getSelectedParagraph() {
        return selectedParagraph;
    }
    
    public void setSelectedParagraph(ParagraphClass initSelectedParagraph) {
        selectedParagraph = initSelectedParagraph;
    }
   
    public ParagraphClass addParagraph() {
        ParagraphClass oneParagraph = new ParagraphClass();
        theParagraphs.add(oneParagraph);
        
        return oneParagraph;
    }
    
    public void removeSelectedParagraph() {
        theParagraphs.remove(selectedParagraph);
    }
}
