/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author ddjump
 * 
 * ListModel = ListClass
 * ListClass = TextFieldClass
 * theList = theTextFields
 */
public class ListClass {
    ObservableList<TextFieldClass> theTextFields;
    TextFieldClass selectedTextField;
    
    public ListClass() {
        theTextFields= FXCollections.observableArrayList();
    }
    
    public ObservableList<TextFieldClass> getTextField() {
        return theTextFields;
    }
    
    public TextFieldClass getSelectedTextField() {
        return selectedTextField;
    }
    
    public void setSelectedTextField(TextFieldClass initSelectedTextField) {
        selectedTextField = initSelectedTextField;
    }
   
    public void addTextField(TextFieldClass dataToAdd) {
        theTextFields.add(dataToAdd);
    }
    
    public void removeSelectedTextField() {
        theTextFields.remove(selectedTextField);
    }
}