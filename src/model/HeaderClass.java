/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author ddjump
 */
public class HeaderClass {
    String header;
    
    public HeaderClass() {
        header = "untitled";
    }
    
    public String getHeader() {
        return header;
    }
    
    public void setHeader(String newHeader) {
        header = newHeader;
    }
}
