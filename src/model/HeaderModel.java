/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author ddjump
 */
public class HeaderModel {
    
    //HeaderClass oneHeader;
    ObservableList<HeaderClass> theHeaders;
    HeaderClass selectedHeader;
    
    public HeaderModel() {
        theHeaders = FXCollections.observableArrayList();
    }
    
    public ObservableList<HeaderClass> getTheHeaders() {
        return theHeaders;
    }
    
    public HeaderClass getSelectedHeader() {
        return selectedHeader;
    }
    
    public void setSelectedHeader(HeaderClass initSelectedHeader) {
        selectedHeader = initSelectedHeader;
    }
   
    public HeaderClass addHeader() {
        System.out.println("a header is added");
        HeaderClass oneHeader = new HeaderClass();
        //oneHeader.setHeader(headerTitle);
        theHeaders.add(oneHeader);
        
        return oneHeader;
    }
    
    public void removeSelectedHeader() {
        theHeaders.remove(selectedHeader);
    }
}
