/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author ddjump
 */
public class ImageModel {
    ObservableList<ImageClass> theImages;
    ImageClass selectedImage;
    
    public ImageModel() {
        theImages = FXCollections.observableArrayList();
    }
    
    public ObservableList<ImageClass> getTheImages() {
        return theImages;
    }
    
    public ImageClass getSelectedImage() {
        return selectedImage;
    }
    
    public void setSelectedImage(ImageClass initSelectedImage) {
        selectedImage = initSelectedImage;
    }
   
    public ImageClass addImage() {
        System.out.println("a Image is added");
        ImageClass oneImage = new ImageClass();
        theImages.add(oneImage);
        
        return oneImage;
    }
    
    public void removeSelectedImage() {
        theImages.remove(selectedImage);
    }
}
